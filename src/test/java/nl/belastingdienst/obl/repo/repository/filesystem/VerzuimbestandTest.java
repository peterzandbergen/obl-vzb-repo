package nl.belastingdienst.obl.repo.repository.filesystem;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

public class VerzuimbestandTest {

    @Test
    public void serde() {
        Verzuimbestand vz = new Verzuimbestand();

        // Serialize
        vz.id = "id";
        byte[] ar = "Hallo daar".getBytes();
        vz.content = ar;

        String json = Verzuimbestand.toJson(vz);

        System.out.println(json);

        try {
            // Deserialize.
            Verzuimbestand deser = Verzuimbestand.fromJson(json);
            System.out.println("deser.id=" + deser.id);
            System.out.println("vz.id=" + vz.id);
            System.out.println(vz.content.toString());
            System.out.println(deser.content.toString());
            assertTrue("Checking id", deser.id.length() == vz.id.length());
            assertTrue("Checking id", deser.id.compareTo(vz.id) == 0);
            assertTrue("Checking content", deser.content.length == vz.content.length);
            assertTrue("Checking content", Arrays.equals(vz.content, deser.content));

            assertTrue(true);
        } catch (Exception exc) {
            assertTrue("Exception" + exc.getMessage(), false);
        }

    }
}
