package nl.belastingdienst.obl.repo.repository.filesystem;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import nl.belastingdienst.obl.repo.model.Verzuimbestand;

public class FileSystemRepoTest {

    @Test
    public void testCreate() {

        try {
            // Path root = Files.createTempDirectory(new File("/tmp").toPath(),
            // "fileSystemRepoTest-");
            Path root = new File("/tmp/testdir").toPath();

            System.out.println("root is: " + root.toString());

            FileSystemRepo repo = new FileSystemRepo(root.toString());

            Verzuimbestand vzb = new Verzuimbestand();
            vzb.filename = "file naam";
            vzb.content = "dit is een hele mooie test".getBytes();

            Verzuimbestand newVzb = repo.create(vzb);
            assertTrue("is is not nill", newVzb.id.length() > 0);
            System.out.println("ID is: " + newVzb.id);
        } catch (Exception exc) {
            assertTrue("Oops: " + exc.getMessage(), false);
        }
    }

    @Test
    public void testGetList() {

        try {
            // Path root = Files.createTempDirectory(new File("/tmp").toPath(),
            // "fileSystemRepoTest-");
            Path root = new File("/tmp/testdir").toPath();

            System.out.println("root is: " + root.toString());

            FileSystemRepo repo = new FileSystemRepo(root.toString());

            Verzuimbestand[] vbs = repo.getList();

            assertTrue("list not empty", vbs.length > 0);

            System.out.println("number of vbs: " + vbs.length);

            for (Verzuimbestand vb : vbs) {
                System.out.println(vb.id);
            }

        } catch (Exception exc) {
            assertTrue("Oops: " + exc.getMessage(), false);
        }
    }

  
      @Test
    public void testGet() {

        try {
            // Path root = Files.createTempDirectory(new File("/tmp").toPath(),
            // "fileSystemRepoTest-");
            Path root = new File("/tmp/testdir").toPath();

            System.out.println("root is: " + root.toString());

            FileSystemRepo repo = new FileSystemRepo(root.toString());

            Verzuimbestand[] vbs = repo.getList();

            assertTrue("list not empty", vbs.length > 0);

            String firstId = vbs[0].id;
            Verzuimbestand vbGet = repo.get(firstId);

            assertTrue("check the ids", vbGet.id.compareTo(firstId) == 0);
            

        } catch (Exception exc) {
            assertTrue("Oops: " + exc.getMessage(), false);
        }
    }

    

    @Test
    public void testCreateWriteContent() {
        try {
            // Path root = Files.createTempDirectory(new File("/tmp").toPath(),
            // "fileSystemRepoTest-");
            Path root = new File("/tmp/testdir").toPath();

            System.out.println("root is: " + root.toString());

            FileSystemRepo repo = new FileSystemRepo(root.toString());

            Verzuimbestand vzb = new Verzuimbestand();
            vzb.filename = "with-content";
            vzb.content = "dit is een hele mooie test".getBytes();

            Verzuimbestand newVzb = repo.create(vzb);
            assertTrue("is is not nill", newVzb.id.length() > 0);
            System.out.println("ID is: " + newVzb.id);

            // Try to get the content.
            InputStream is = repo.getContent(newVzb.id);
            String content = new String(is.readAllBytes(), StandardCharsets.UTF_8);
            is.close();
            System.out.printf("content is: %s\n", content);
        } catch (Exception exc) {
            assertTrue("Oops: " + exc.getMessage(), false);
        }
    }



    @Test
    public void testGetWithContent() {
        try {
            // Path root = Files.createTempDirectory(new File("/tmp").toPath(),
            // "fileSystemRepoTest-");
            Path root = new File("/tmp/testdir").toPath();

            System.out.println("root is: " + root.toString());

            FileSystemRepo repo = new FileSystemRepo(root.toString());

            Verzuimbestand newVzb = repo.get("6d701717-a525-4ed5-966d-bddddc80476b");

            // Try to get the content.
            InputStream is = repo.getContent(newVzb.id);
            String content = new String(is.readAllBytes(), StandardCharsets.UTF_8);
            is.close();
            System.out.printf("content is: \n%s\n", content);
        } catch (Exception exc) {
            assertTrue("Oops: " + exc.getMessage(), false);
        }
    }


    
}

