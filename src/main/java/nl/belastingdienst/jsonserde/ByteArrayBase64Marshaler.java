package nl.belastingdienst.jsonserde;

import java.lang.reflect.Type;

import com.google.gson.GsonBuilder;
// import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonParseException;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.InstanceCreator;

import java.util.Base64;

public class ByteArrayBase64Marshaler implements JsonSerializer<byte[]>, JsonDeserializer<byte[]>, InstanceCreator<byte[]> {

    public static void registerMarshaler(GsonBuilder gson) {
        gson.disableHtmlEscaping().registerTypeAdapter(byte[].class, new ByteArrayBase64Marshaler());
    }

    @Override
    public byte[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {

        try {
            // Convert the base64 string to a byte array
            String s = json.getAsString();
            byte[] b = Base64.getDecoder().decode(s);
            return b;
        } catch (Exception exc) {
            throw new JsonParseException("Failed to deserialize byte array.", exc);
        }
    }

    @Override
    public JsonElement serialize(byte[] src, Type typeOfSrc, JsonSerializationContext context) {
        // TODO Auto-generated method stub
        try {
            // String s = Base64.getEncoder().withoutPadding().encodeToString(src);
            String s = Base64.getEncoder().encodeToString(src);
            return new JsonPrimitive(s);
        } catch (Exception exc) {
            throw new JsonParseException("Failed to serialize byte array.", exc);
        }
    }

    @Override
    public byte[] createInstance(Type type) {
        // TODO Auto-generated method stub
        return new byte[0];
    }

}
