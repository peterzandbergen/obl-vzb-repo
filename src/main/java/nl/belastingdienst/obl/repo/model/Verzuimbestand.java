package nl.belastingdienst.obl.repo.model;

public class Verzuimbestand {
    public String id;
    public String status;
    public String filename;
    public byte[] content;

    public Verzuimbestand() {}

    public void setStatus(String status) {
        this.status = status;
    }
}
