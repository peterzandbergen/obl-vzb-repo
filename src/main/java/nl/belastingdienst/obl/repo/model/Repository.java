package nl.belastingdienst.obl.repo.model;

import java.io.InputStream;
import java.io.OutputStream;

public interface Repository {

    Verzuimbestand[] getList() throws Exception;

    Verzuimbestand get(String id) throws Exception;

    Verzuimbestand create(Verzuimbestand verzuimbestand) throws Exception;

    Verzuimbestand update(Verzuimbestand verzuimbestand);

    void delete(String id);

    InputStream getContent(String id) throws Exception;

    OutputStream setContent(String id)  throws Exception;

}
