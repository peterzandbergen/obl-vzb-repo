package nl.belastingdienst.obl.repo;

import java.nio.charset.StandardCharsets;

import nl.belastingdienst.obl.repo.repository.filesystem.Verzuimbestand;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Verzuimbestand vz = new Verzuimbestand();
        vz.id = "id1";
        byte[] ar = "Hallo daar".getBytes(StandardCharsets.UTF_8);
        vz.content = ar;

        String json = Verzuimbestand.toJson(vz);

        System.out.println(json);
    }
}
