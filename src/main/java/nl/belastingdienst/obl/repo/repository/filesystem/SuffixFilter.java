package nl.belastingdienst.obl.repo.repository.filesystem;

import java.io.File;
import java.io.FilenameFilter;

public class SuffixFilter implements FilenameFilter {

    public String suffix;

    SuffixFilter(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public boolean accept(File dir, String name) {
        return name.endsWith(this.suffix);
    }
    
}
