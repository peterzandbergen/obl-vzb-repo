package nl.belastingdienst.obl.repo.repository.filesystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import com.google.gson.Gson;

// import nl.belastingdienst.obl.repo.model.Repository;
// import nl.belastingdienst.obl.repo.model.Verzuimbestand;

public class FileSystemRepo implements nl.belastingdienst.obl.repo.model.Repository {

    public static final String METADATA_SUFFIX = "metadata";

    // Root directory of the repository
    File root;
    Gson marshaller;
    String suffix;
    SuffixFilter suffixFilter;

    public FileSystemRepo(String rootPath) throws Exception {
        // Try to create the root
        File r = new File(rootPath);

        // Must be a directory and must exist.
        if (!r.isDirectory()) {
            throw new Exception("root is not a directory");
        }

        // All is well.
        this.root = r;
        this.suffix = FileSystemRepo.METADATA_SUFFIX;
        this.suffixFilter = new SuffixFilter("." + this.suffix);
        this.marshaller = JsonMarshaler.getGson();
    }

    @Override
    public nl.belastingdienst.obl.repo.model.Verzuimbestand get(String id) throws Exception {
        // Read the metadata file content.
        File f = getMetadataFile(id);

        // Unmarshal
        Verzuimbestand vzb = this.getFromFile(f);

        return vzb.toModel();
    }

    @Override
    public nl.belastingdienst.obl.repo.model.Verzuimbestand create(
            nl.belastingdienst.obl.repo.model.Verzuimbestand verzuimbestand) throws Exception {
        // Create a verzuimbestand.
        Verzuimbestand vzb = Verzuimbestand.fromModel(verzuimbestand);

        // Filter out fields.
        // Create a new id.
        vzb.id = newId();
        // Delete the filename.
        vzb.filename = verzuimbestand.filename;
        // Set the status
        vzb.status = "NEW";
        // Do not copy the content.
        vzb.content = null;
        vzb.created = new Date();

        // Save the content if present.
        if (verzuimbestand.content != null && verzuimbestand.content.length > 0) {
            // Create the content file.
            vzb.internalFilename = this.createUniqueContent(verzuimbestand.content, verzuimbestand.filename);
            vzb.status = "HAS_CONTENT";
        }


        String json = Verzuimbestand.toJson(vzb);

        File md = new File(this.root, vzb.id + "." + this.suffix);
        Files.writeString(md.toPath(), json, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);

        return vzb.toModel();
    }

    @Override
    public nl.belastingdienst.obl.repo.model.Verzuimbestand update(
            nl.belastingdienst.obl.repo.model.Verzuimbestand verzuimbestand) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void delete(String id) {
        // TODO Auto-generated method stub

    }

    @Override
    public InputStream getContent(String id) throws Exception {

        try {
            // Read the metadata file content.
            File f = getMetadataFile(id);

            // Unmarshal
            Verzuimbestand vzb = this.getFromFile(f);

            // Get the data file.
            File content = new File(this.root, vzb.internalFilename);

            // Create input stream.
            return new FileInputStream(content);

        } catch (Exception exc) {
            throw new Exception("error creating InputStream", exc);
        }

        // Get the verzuimbestand.

    }

    @Override
    public OutputStream setContent(String id) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public nl.belastingdienst.obl.repo.model.Verzuimbestand[] getList() throws Exception {
        // Get directory listing with glob.
        File[] files = root.listFiles(this.suffixFilter);

        // Read the metadata.
        Verzuimbestand[] result = new Verzuimbestand[files.length];
        for (int i = 0; i < files.length; i++) {
            result[i] = this.getFromFile(files[i]);
        }

        Arrays.sort(result);

        return Verzuimbestand.toModel(result);
    }

    File getMetadataFile(String id) {
        return new File(this.root, id + "." + this.suffix);
    }

    Verzuimbestand getFromFile(File file) throws Exception {
        if (!file.isFile()) {
            throw new Exception("not found");
        }

        // Read the content of the file.
        String json = Files.readString(file.toPath());

        // Unmarshal
        return this.marshaller.fromJson(json, Verzuimbestand.class);
    }

    public String newId() {
        return UUID.randomUUID().toString();
    }

    String createUniqueContent(byte[] content, String filename) throws Exception {

        boolean done = false;

        String fn = filename;

        int i = 0;
        while (!done) {
            File f = new File(this.root, fn);
            if (f.exists()) {
                fn = String.format("%s-%04d", filename, i);
                i++;
                continue;
            }

            // Filename should be unique, but we can have race issues.
            try {
                this.createNewContent(content, fn);
                done = true;
            } catch (Exception exc) {
            }
        }
        return fn;
    }

    void createNewContent(byte[] content, String filename) throws IOException {

        File f = new File(this.root, filename);
        Files.write(f.toPath(), content, StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
    }

}
