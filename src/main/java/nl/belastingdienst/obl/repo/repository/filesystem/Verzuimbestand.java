package nl.belastingdienst.obl.repo.repository.filesystem;

import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;


public class Verzuimbestand implements Comparable<Verzuimbestand> {
   public String id;
   public String status;
   public String filename;
   public byte[] content;
   public String internalFilename;
   public Date created;


   public static String toJson(Verzuimbestand verzuimbestand) {
      Gson gs = JsonMarshaler.getGson();
      // Gson gs = new GsonBuilder().create();

      String json = gs.toJson(verzuimbestand);
      return json;
   }

   public static Verzuimbestand fromJson(String json) throws Exception {
      Gson gs = JsonMarshaler.getGson();
      // Gson gs = new GsonBuilder().create();

      try {
         Verzuimbestand vz = gs.fromJson(json, Verzuimbestand.class);
         return vz;
      } catch (JsonSyntaxException je) {
         throw new Exception("Cannot unmarshal Verzuimbestand", je);
      }
   }

   public nl.belastingdienst.obl.repo.model.Verzuimbestand toModel()  {
      nl.belastingdienst.obl.repo.model.Verzuimbestand m = new nl.belastingdienst.obl.repo.model.Verzuimbestand();
      m.id = this.id;
      m.status = this.status;
      m.filename = this.filename;
      m.content = this.content;
      return m;
   }

   public static Verzuimbestand fromModel(nl.belastingdienst.obl.repo.model.Verzuimbestand model) {
      Verzuimbestand vzb = new Verzuimbestand();
      vzb.id = model.id;
      vzb.status = model.status;
      vzb.filename = model.filename;
      vzb.content = model.content;
      return vzb;
   }

   public static nl.belastingdienst.obl.repo.model.Verzuimbestand[] toModel(Verzuimbestand[] list) {
      nl.belastingdienst.obl.repo.model.Verzuimbestand[] result = new nl.belastingdienst.obl.repo.model.Verzuimbestand[list.length];
      for ( int i = 0; i < list.length; i++) {
         result[i] = list[i].toModel();
      }
      return result;
   }

   @Override
   public int compareTo(Verzuimbestand o) {
      
      return this.filename.compareTo(o.filename);
   }
}
