package nl.belastingdienst.obl.repo.repository.filesystem;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import nl.belastingdienst.jsonserde.ByteArrayBase64Marshaler;

public class JsonMarshaler {

    static Gson gson;

    static Gson getGson() {
        if (gson == null) {
            GsonBuilder gb = new GsonBuilder();
            ByteArrayBase64Marshaler.registerMarshaler(gb);
            gson = gb.create();
        }
        return gson;
    }
}
