# Maven cheat sheet

## Build super jar

```bash
mvn mvn clean compile assembly:single
```

## Run the super jar

```bash
java -cp ./target/test-app-1.0-SNAPSHOT-jar-with-dependencies.jar nl.belastingdienst.obl.repo.App
```


